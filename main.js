function createNewUser() {
  firstName = prompt("Enter your last name", "");
  lastName = prompt("Enter your first name", "");

  let newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin(firstName, lastName) {
      return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
    },
    setNewFirstName(newFName) {
      Object.defineProperty(newUser, "firstName", {
        value: newFName,
      });
    },
    setNewLastName(newLName) {
      Object.defineProperty(newUser, "lastName", {
        value: newLName,
      });
    },
  };

  Object.defineProperties(newUser, {
    firstName: {
      writable: false,
      configurable: true,
    },
    lastName: {
      writable: false,
      configurable: true,
    },
  });

  return newUser;
}

let myUser = createNewUser();

console.log(myUser.getLogin());

//Тестирование задания повышенной сложности
myUser.firstName = "TEST1";
myUser.lastName = "TEST2";
console.log(myUser);

myUser.setNewFirstName("TEST1");
myUser.setNewLastName("TEST2");
console.log(myUser);

